package org.revenant.advanalytics;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;

@Controller
public class RestApiController {
    @ResponseBody
    @GetMapping("/getOffers")
    List<OfferForWebClient> offers() {
        return getTopOffers();
    }


    private List<OfferForWebClient> getTopOffers(){

        List<Offer> offerList = fetchOffers();
        offerList.sort((o1, o2) -> {
            BigDecimal id1 = getRubleSum(o1.getPayments());
            BigDecimal id2 = getRubleSum(o2.getPayments());
            if (id1 == null) {
                return id2 == null ? 0 : 1;
            }
            if (id2 == null) {
                return -1;
            }
            return id2.compareTo(id1);
        });
        List<OfferForWebClient> result = new ArrayList<>();
        for (Offer offer :
                offerList.subList(0,20)) {
            result.add(new OfferForWebClient(offer.getId()
                    ,offer.getTitle()
                    ,offer.getLogo()
                    ,offer.getCategories()
                    ,getRubleSum(offer.getPayments())
                    ,"RUB"));
        }

        return result;
    }
    private BigDecimal getRubleSum(List<Payment> payments){
        BigDecimal result = new BigDecimal(0.0);
        for (Payment payment :
                payments) {
            switch (payment.getCurrency()) {
                case "RUB":
                    result = result.add(payment.getRevenue());
                    break;
                case "USD":
                    result = result.add(payment.getRevenue().multiply(new BigDecimal(60)));
                    break;
                case "EUR":
                    result = result.add(payment.getRevenue().multiply(new BigDecimal(65)));
                    break;
            }
        }
        return result;
    }

    private List<Offer> fetchOffers(){
        RestTemplate restTemplate = new RestTemplate();
        List<Offer> result = new ArrayList<>();
        int i = 1;
        RequestObject requestObject = restTemplate.getForObject("http://api.omnia.online/2.1/offers?page="+i,RequestObject.class);
        result.addAll(requestObject.getOffer());
        while (true){
            if(requestObject.getPagination().getNextPage()!=null){
                ++i;
                requestObject = restTemplate.getForObject("http://api.omnia.online/2.1/offers?page="+i,RequestObject.class);
                result.addAll(requestObject.getOffer());
            } else {
                break;
            }
        }

        return result;
    }

}
