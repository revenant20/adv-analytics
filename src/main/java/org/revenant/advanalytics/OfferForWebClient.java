package org.revenant.advanalytics;


import java.math.BigDecimal;
import java.util.List;

public class OfferForWebClient {

    private Integer id;
    private String title;
    private String logo;
    private List<String> categories;
    private BigDecimal revenue;
    private String currency;

    public OfferForWebClient(Integer id, String title, String logo, List<String> categories, BigDecimal revenue, String currency) {
        this.id = id;
        this.title = title;
        this.logo = logo;
        this.categories = categories;
        this.revenue = revenue;
        this.currency = currency;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLogo() {
        return logo;
    }

    public List<String> getCategories() {
        return categories;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public String getCurrency() {
        return currency;
    }


}
