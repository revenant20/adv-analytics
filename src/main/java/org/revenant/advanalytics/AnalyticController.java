package org.revenant.advanalytics;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AnalyticController {

    @GetMapping("/analytics")
    String getAnalyticPage (){
        return "index.html";
    }

}
