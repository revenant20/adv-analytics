package org.revenant.advanalytics;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestObject {
    private Integer status;
    private List<Offer> offer;
    private Pagination pagination;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Offer> getOffer() {
        return offer;
    }
    @JsonProperty("offers")
    public void setOffer(List<Offer> offer) {
        this.offer = offer;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "RequestObject{" +
                "status=" + status +
                ", offer=" + offer +
                ", pagination=" + pagination +
                '}';
    }
}
