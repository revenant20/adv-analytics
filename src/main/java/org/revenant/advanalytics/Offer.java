package org.revenant.advanalytics;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.boot.jackson.JsonComponent;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Offer {

    private Integer id;
    private String title;
    private String logo;
    private List<String> categories;

    private List<Payment> payments;

    public Offer() {

    }

    public Offer(Integer id, String title, String logo, List<String> categories, List<Payment> payments) {
        this.id = id;
        this.title = title;
        this.logo = logo;
        this.categories = categories;
        this.payments = payments;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLogo() {
        return logo;
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", logo='" + logo + '\'' +
                ", categories=" + categories +
                ", payments=" + payments +
                '}';
    }
}
