package org.revenant.advanalytics;

import org.springframework.boot.jackson.JsonComponent;

import java.math.BigDecimal;


public class Payment {
    private BigDecimal revenue;
    private String currency;

    public Payment() {
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Payment(BigDecimal revenue, String currency) {
        this.revenue = revenue;
        this.currency = currency;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "revenue=" + revenue +
                ", currency='" + currency + '\'' +
                '}';
    }
}
