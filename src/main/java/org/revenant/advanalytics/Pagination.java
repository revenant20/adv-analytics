package org.revenant.advanalytics;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagination {
    private Integer perPage;
    private Integer totalCount;
    private Integer page;
    private Integer prevPage;
    private Integer nextPage;
    @JsonProperty("per_page")
    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    @JsonProperty("total_count")
    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @JsonProperty("page")
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @JsonProperty("prev_page")
    public Integer getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(Integer prevPage) {
        this.prevPage = prevPage;
    }

    @JsonProperty("next_page")
    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "perPage=" + perPage +
                ", totalCount=" + totalCount +
                ", page=" + page +
                ", prevPage=" + prevPage +
                ", nextPage=" + nextPage +
                '}';
    }
}
