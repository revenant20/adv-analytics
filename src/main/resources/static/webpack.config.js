module.exports = {
    entry: {
               app: "./app/main.ts",
               vendor: "./app/vendor.ts",
               polyfill: "./app/polyfill.ts"
    },
    output: {
        path: __dirname,
        filename: "[name].bundle.js"
    },
    module: {
        loaders: [
    {
      test: /\.css$/,
      //include: helpers.root('src', 'app'),
      loader: 'raw'
    },
            //{test: /\.css$/, loader: "style!css"},
            {test: /\.ts$/, loader: "ts-loader" },
      /*
      {
        test: /\.ts$/,
        loaders: ['awesome-typescript-loader', 'angular2-template-loader'],
        exclude: [/\.(spec|e2e)\.ts$/]
      },
      */
          {
            test: /\.html$/,
            loader: 'html'
          }
        ]
    },
    resolve: {
      extensions: ['', '.ts', '.js']
    }
};
