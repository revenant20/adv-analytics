export class Offer{
    id: number;
    title: string;
    logo: string;
    categories: string[];
    revenue: number;
    currency: string;
}
export class Payment{
    revenue: number;
    currency: string;
}