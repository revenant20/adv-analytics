import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';



import { AppComponent }   from './app.component';
import {RouterModule} from "@angular/router";
import {AnalyticsComponent} from './analytic.component';

@NgModule({
    imports:      [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo:'/analytics',
                pathMatch: 'full'
            },
            {
              path: 'analytics',
                component: AnalyticsComponent
            }
        ])
    ],
    declarations: [
        AppComponent,
        AnalyticsComponent
    ],

    bootstrap:    [ AppComponent]
})
export class AppModule {



}

