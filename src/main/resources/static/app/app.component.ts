import {Component, OnInit} from '@angular/core';
import of = require("core-js/fn/array/of");
import {Router, Event} from "@angular/router";



@Component({
    //moduleId: module.id,

    selector: 'my-app',
    template: require('./app.component.html'),
    styles: [require('./app.component.css')],
})
export class AppComponent implements OnInit {
    url: string ='';
    isSessionNotEmpty: boolean;
    isReturnToUserList: boolean;
    urlsWithoutGoToUserListButton = [
    '/login',
    '/registration',
    '/userList',
    '/successfulRegistration',
    '/blockedMassage'
    ];

    constructor( private router: Router) { }

    ngOnInit(): void {
        this.router.events.subscribe((event:Event) => {
            //if(event instanceof NavigationStart) {
            //}
            // NavigationEnd
            // NavigationCancel
            // NavigationError
            // RoutesRecognized
            this.url = this.router.url;

        });

    }



}

