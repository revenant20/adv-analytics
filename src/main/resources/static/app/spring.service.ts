import {Offer} from "./offer";
import {Http} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class SpringOfferService{
    constructor(private http: Http){}
    getOffers() : Promise<Offer[]>{
        return this.http.get('/getOffers')
            .toPromise()
            .then(response=> response.json() as Offer[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}