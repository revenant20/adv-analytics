import {Component, OnInit} from '@angular/core';
import {SpringOfferService} from "./spring.service";
import {Payment,Offer} from "./offer";

@Component({
    //moduleId: module.id,
    selector: 'analytics',
    template: require('./analytic.component.html'),
    styles: [require('./analytic.component.css')],
    providers: [SpringOfferService]
})


export class AnalyticsComponent extends OnInit{
    offers: Offer[]= [];


    ngOnInit(): void {
        this.service.getOffers().then(offers=>{
            this.offers = offers;
        });
    }
    constructor(
        private service: SpringOfferService
    ){super();};
}
